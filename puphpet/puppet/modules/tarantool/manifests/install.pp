
class tarantool::install($package_name='tarantool', $package_version='latest') {
# $::lsbdistcodename is blank in CentOS


  if $::operatingsystem == 'ubuntu' {
  }

  case $::operatingsystem {
    'Ubuntu', 'Debian': {
      $opsys = downcase($::operatingsystem)
      apt::key { 'BF202DC9': key_source => 'http://tarantool.org/dist/public.key' }
      apt::source { 'tarantool':
        location => "http://tarantool.org/dist/master/${opsys}/",
        require => Apt::Key['BF202DC9']
      }
    }
    default:            {}
  }


  package { $package_name:
    ensure => $package_version,
  }
}
