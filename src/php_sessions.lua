local php_sessions = {}

function php_sessions.init(space_name, user, pass)
    local space = box.space[space_name]
    local space_exists = space ~= nil
    if not space_exists then
        space = box.schema.space.create(space_name)
        space:create_index("primary", {type = 'hash', parts = {1, 'STR'}}) -- session id
        space:create_index("secondary", {type = 'tree', parts = {2, 'NUM'}}) -- session create/update timestamp
    end

    local user_exist = box.schema.user.exists(user)

    if not user_exist then
        box.schema.user.create(user, {password=pass})
    end

    if not space_exists or not user_exist then
        box.schema.user.grant(user, 'read,write', 'space', space_name)
    end

    local gc_func_name = 'php_sessions.gc'
    local gc_func_exists = box.schema.func.exists(gc_func_name)
    box.schema.func.create('php_sessions.gc', {if_not_exists=true})

    if not gc_func_exists or not user_exist then
        box.schema.user.grant(user, 'execute', 'function', gc_func_name)
    end
end

function php_sessions.gc(space, maxlifetime)
    local tuple
    cnt = 0

    timestamp = math.max(os.time() - maxlifetime, 1)

    for _,tuple in box.space[space].index.secondary:pairs({timestamp}, {iterator = 'LT'}) do
        cnt = cnt + 1
        box.space[space]:delete(tuple[1])
    end
    return cnt
end

return php_sessions