<?php
namespace Tarantool;

class SessionHandler implements \SessionHandlerInterface
{
    /** @var \Tarantool */
    private $tarantool;

    /** @var string */
    private $sessionKeyPrefix;

    private $gcFunctionName = 'php_sessions.gc';

    /**
     * SessionHandler constructor.
     * @param string $host
     * @param int $port
     * @param string $user
     * @param string $password
     * @param string $space
     */
    public function __construct($host, $port, $user, $password, $space)
    {
        $this->tarantool = new \Tarantool($host, $port);
        $this->tarantool->connect();
        $this->tarantool->authenticate($user, $password);
        $this->space = $space;
    }

    /**
     * @param string $host
     * @param int $port
     * @param string $user
     * @param string $password
     * @param string $space
     * @return self
     */
    public static function register($host, $port, $user, $password, $space)
    {
        $instance = new self($host, $port, $user, $password, $space);
        session_set_save_handler($instance, true);
        return $instance;
    }

    /**
     * @param string $gcFunctionName
     * @return SessionHandler
     */
    public function setGcFunctionName($gcFunctionName)
    {
        $this->gcFunctionName = $gcFunctionName;
        return $this;
    }

    public function create_sid()
    {
        mt_srand((double)microtime()*10000+mt_rand());
        return sha1(uniqid(mt_rand(), true));
    }

    /**
     * @inheritdoc
     */
    public function open($save_path, $session_key)
    {
        $this->sessionKeyPrefix = $save_path . '^' . $session_key;
    }

    /**
     * @inheritdoc
     */
    public function close()
    {
        $this->sessionKeyPrefix = null;
    }

    /**
     * @inheritdoc
     */
    public function destroy($session_id)
    {
        $this->tarantool->delete($this->space, [$this->makeKey($session_id)]);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function gc($maxlifetime)
    {
        $this->tarantool->call($this->gcFunctionName, [$this->space, $maxlifetime]);
    }

    /**
     * @inheritdoc
     */
    public function read($session_id)
    {
        $data = $this->tarantool->select($this->space, $this->makeKey($session_id));
        return count($data) === 1 ? $data[0][2] : null;
    }

    /**
     * @inheritdoc
     */
    public function write($session_id, $session_data)
    {
        try {
            return (bool)$this->tarantool->replace($this->space, [$this->makeKey($session_id), time(), $session_data]);
        } catch (\Exception $e) {
            error_log('[warning] can not write session to tarantool: ' . (string)$e);
            return false;
        }
    }

    /**
     * @param string $session_id
     * @return string
     */
    private function makeKey($session_id)
    {
        return $this->sessionKeyPrefix . '@' . $session_id;
    }
}