<?php

if (!preg_match('@^/(\?|$)@', $_SERVER['REQUEST_URI'])) die; // skip /favicon.ico and other

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 1);
ini_set('session.gc_maxlifetime', 10);

require_once __DIR__ . '/../vendor/autoload.php';

function l($msg) {
    echo '# ', $msg, PHP_EOL;
}

echo '<pre>';

\Tarantool\SessionHandler::register('localhost', '3301', 'php_sessions', '12345', 'php_sessions');
l('Handler registered');

session_start();
l('Session started with id ' . session_id());

if (!empty($_GET['destroy'])) {
    session_destroy();
    l('Session destroyed');
    exit();
}

if (!empty($_SESSION)) {
    l('Session current data: ' . var_export($_SESSION, true));
} else {
    l('Session current data is empty');
}

if (!isset($_SESSION['data'])) {
    $_SESSION['data'] = [[], 0];
}

$_SESSION['data'][0] = date('d.m.Y H:i:s');
$_SESSION['data'][1]++;

l('Session new data: ' . var_export($_SESSION, true));

echo '</pre>';